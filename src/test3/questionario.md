## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

2. Quais foram os últimos dois framework javascript que você trabalhou?

3. Descreva os principais pontos positivos de seu framework favorito.

4. Descreva os principais pontos negativos do seu framework favorito.

5. O que é código de qualidade para você?


## Conhecimento de desenvolvimento

1. O que é GIT?

2. Descreva um workflow simples de trabalho utilizando GIT.

3. Como você avalia seu grau de conhecimento em Orientação a objeto?

4. O que é Dependency Injection?

5. O significa a sigla S.O.L.I.D?

6. O que é BDD e qual sua importância para o processo de desenvolvimento?

7. Fale sobre Design Patterns.

9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
